﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour {

    [SerializeField]
    private GameObject Game_Controller;
    [SerializeField]
    private GameObject ScoreGameTxt;
    [SerializeField]
    private GameObject ScoreWinTxt;

    [SerializeField]
    private float scoreToBeAdded;
    private float currentScore = 0.0f;

    private AudioSource[] audiosources;
    private bool soundTrigger;

	// Use this for initialization
	void Start () {
        ScoreGameTxt.GetComponent<Text>().text = currentScore.ToString();
        audiosources = this.GetComponents<AudioSource>();
    }

    // Update is called once per frame
    void Update () {
        if (Game_Controller.GetComponent<GoogleAdManager>().isShowingBanner) {
            ScoreGameTxt.transform.localPosition = new Vector3(0, 475, 0);
        } else {
            ScoreGameTxt.transform.localPosition = new Vector3(0, 500, 0);

        }
    }

    public void AddScore() {
        // Check for achievments, if so then add achievment and show the user this
        currentScore += scoreToBeAdded;
        ScoreGameTxt.GetComponent<Text>().text = currentScore.ToString();
        if (soundTrigger) {
            audiosources[0].Play();
            soundTrigger = false;
        } else {
            audiosources[1].Play();
            soundTrigger = true;
        }

        Game_Controller.GetComponent<GooglePlayManager>().UnlockPossibleAchievement(currentScore);
    }

    public void RestartScore() {
        currentScore = 0.0f;
        ScoreGameTxt.GetComponent<Text>().text = currentScore.ToString();
        ScoreWinTxt.GetComponent<Text>().text = "0";
    }

    public void SaveGameScore() {
        audiosources[2].Play();
        ScoreWinTxt.GetComponent<Text>().text = currentScore.ToString();
        Game_Controller.GetComponent<GooglePlayManager>().AddScoreToLeaderboard(currentScore);
    }

    public string GetWinScoreString() {
        return currentScore.ToString();
    }
}
