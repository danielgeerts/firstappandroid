﻿using UnityEngine;
using System.Collections;

public class ScoreConfig : MonoBehaviour {

    [SerializeField]
    private GameObject GameController;
    [SerializeField]
    private GameObject ScoreController;
    [SerializeField]
    private GameObject CoinController;

    private float delay = 0.075f;
    private float counter;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnCollisionEnter(Collision other) {
        if (other.gameObject.tag.Equals("TrowBall") && !other.gameObject.GetComponent<BallMovement>().isTurned) {
            GameController.gameObject.GetComponent<ColorController>().changeColor();
            ScoreController.GetComponent<ScoreController>().AddScore();
            other.gameObject.GetComponent<BallMovement>().Turn();
            counter = 0;
        }
    }

    public void OnCollisionStay(Collision other) {
        if (other.gameObject.tag.Equals("TrowBall")) {
            counter += 2 * Time.deltaTime;
            if (counter >= delay) {
                Vector3 temp = other.gameObject.transform.position;
                temp.x += Time.deltaTime;
                other.gameObject.transform.position = temp;
            }
        }
    }
}
