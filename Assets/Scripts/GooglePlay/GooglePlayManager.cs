﻿using UnityEngine;
using System.Collections;
using GooglePlayGames.BasicApi;
using UnityEngine.EventSystems;
using GooglePlayGames;
using UnityEngine.UI;
using System;

public class GooglePlayManager : MonoBehaviour {

    [SerializeField]
    private GameObject authObj;

    [SerializeField]
    private GameObject LoginGoogleBtn;
    [SerializeField]
    private Sprite LoginSprite;
    [SerializeField]
    private Sprite LogoutSprite;

    private Text signInButtonText;
    private Text authStatus;

    // Use this for initialization
    void Start () {
    
        // Create client configuration
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();

        // Enable debugging output (recommended)
        PlayGamesPlatform.DebugLogEnabled = true;

        // Initialize and activate the platform
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();
        // END THE CODE TO PASTE INTO START

        SignIn(false);

        authStatus = authObj.GetComponent<Text>();

        //PlayGamesPlatform.Instance.Authenticate(SignInCallback, true);
        PlayGamesPlatform.Instance.SetDefaultLeaderboardForUI("CgkIn6CUicQfEAIQAQ");
    }

    // Update is called once per frame
    void Update () {
        if (PlayGamesPlatform.Instance.localUser.authenticated) {
            LoginGoogleBtn.GetComponent<Image>().sprite = LogoutSprite;
        } else {
            LoginGoogleBtn.GetComponent<Image>().sprite = LoginSprite;

        }
    }

    public void OpenAchievements() {
        if (PlayGamesPlatform.Instance.localUser.authenticated) {
            PlayGamesPlatform.Instance.ShowAchievementsUI();
        } else {
            Debug.Log("Cannot show Achievements, not logged in");
        }
    }

    public void OpenLeaderboard() {
        if (PlayGamesPlatform.Instance.localUser.authenticated) {
            PlayGamesPlatform.Instance.ShowLeaderboardUI("CgkIn6CUicQfEAIQAQ");
        } else {
            Debug.Log("Cannot show leaderboard: not authenticated");
        }
    }

    public void UnlockPossibleAchievement(float score) {
        if (score >= 10) {
            if (PlayGamesPlatform.Instance.localUser.authenticated) {
                string achievementToUnlock = "";

                if (score >= 100) {
                    achievementToUnlock = "CgkIn6CUicQfEAIQAw";
                } else if (score >= 90) {
                    achievementToUnlock = "CgkIn6CUicQfEAIQBw";
                } else if (score >= 80) {
                    achievementToUnlock = "CgkIn6CUicQfEAIQCg";
                } else if (score >= 70) {
                    achievementToUnlock = "CgkIn6CUicQfEAIQCw";
                } else if (score >= 60) {
                    achievementToUnlock = "CgkIn6CUicQfEAIQDQ";
                } else if (score >= 50) {
                    achievementToUnlock = "CgkIn6CUicQfEAIQDA";
                } else if (score >= 40) {
                    achievementToUnlock = "CgkIn6CUicQfEAIQCQ";
                } else if (score >= 30) {
                    achievementToUnlock = "CgkIn6CUicQfEAIQCA";
                } else if (score >= 20) {
                    achievementToUnlock = "CgkIn6CUicQfEAIQBg";
                } else if (score >= 10) {
                    achievementToUnlock = "CgkIn6CUicQfEAIQBA";
                } else {
                    achievementToUnlock = "";
                }

                if (!achievementToUnlock.Equals("")) {
                    PlayGamesPlatform.Instance.ReportProgress(achievementToUnlock, 100,
                        (bool success) => {
                            Debug.Log(PlayGamesPlatform.Instance.localUser.userName + ", Leaderboard update success: " + success);
                        });
                }
            }
        }
    }

    public void AddScoreToLeaderboard(float score) {
        if (PlayGamesPlatform.Instance.localUser.authenticated) {
            int endscore = Mathf.RoundToInt(score);
            PlayGamesPlatform.Instance.ReportScore(endscore, "CgkIn6CUicQfEAIQAQ",
                (bool success) => {
                    Debug.Log(PlayGamesPlatform.Instance.localUser.userName + ", Leaderboard update success: " + success);
                });

            UnlockPossibleAchievement(score);
        }
    }

    public void SignInCallback(bool success) {
        if (authStatus != null) {
            if (success) {
                // Show the user's name
                authStatus.text = "Signed in as: " + Social.localUser.userName;
            } else {
                authStatus.text = "Sign-in failed";
            }
        }
    }

    public void SignIn(bool withoutPopup) {
        if (!PlayGamesPlatform.Instance.localUser.authenticated) {
            // Sign in with Play Game Services, showing the consent dialog
            // by setting the second parameter to isSilent=false.
            PlayGamesPlatform.Instance.Authenticate(SignInCallback, withoutPopup);
        }
    }

    public void SignInOrOut() {
        if (!PlayGamesPlatform.Instance.localUser.authenticated) {
            PlayGamesPlatform.Instance.Authenticate(SignInCallback, false);
            authStatus.text = "Signing in...";
        } else {
            PlayGamesPlatform.Instance.SignOut();
            authStatus.text = "Signed out";
        }
    }
}
