﻿using UnityEngine;
using System.Collections;

public class SoundOnOff : MonoBehaviour {

    [SerializeField]
    private GameObject soundImg;

	// Use this for initialization
	void Start () {
        string temp = SaveTxt.Instance().Load("SoundOnOff");
        if (temp.Contains("False") || temp.Equals("")) {
            SaveTxt.Instance().Save("SoundOnOff", "False");
            soundImg.SetActive(false);
            AudioListener.volume = 1.0f;
        } else {
            soundImg.SetActive(true);
            AudioListener.volume = 0.0f;
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void TurnSoundOnOrOff() {
        if (soundImg.activeSelf) {
            soundImg.SetActive(false);
            AudioListener.volume = 1.0f;
        } else {
            soundImg.SetActive(true);
            AudioListener.volume = 0.0f;
        }

        SaveTxt.Instance().Save("SoundOnOff", soundImg.activeSelf.ToString());
    }
}
