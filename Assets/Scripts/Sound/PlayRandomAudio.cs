﻿using UnityEngine;
using System.Collections;

public class PlayRandomAudio : MonoBehaviour {

    private AudioSource[] audios;
    private int count = 0;

    // Use this for initialization
    void Start () {
        audios = this.GetComponents<AudioSource>();
    }

    public void PlayNextAudio() {
        count++;
        if (count >= audios.Length) {
            count = 0;
        }
        audios[count].Play();
    }
}
