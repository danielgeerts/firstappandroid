﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CoinSpawner : MonoBehaviour {

    [SerializeField]
    private GameObject coin_prefab;
    [SerializeField]
    private GameObject gamecontroller;
    [SerializeField]
    private Vector2 delayRange;
    [SerializeField]
    private int maxCoinToSpawn = 2;
    private int maxCoinCounter = 0;

    private List<GameObject> all_coins;

    private float counter = 0.0f;
    private float delay;

    private Vector3 startPositionPrefab;


    // Use this for initialization
    void Start () {
        delay = Random.Range(delayRange.x, delayRange.y);

        all_coins = new List<GameObject>();

        startPositionPrefab = coin_prefab.transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        if (gamecontroller.GetComponent<GameController>().canSpawnCoin) {
            if (maxCoinCounter < maxCoinToSpawn) {
                counter += 1 * Time.deltaTime;
                if (counter >= delay) {
                    spawnNewCoin();

                    counter = 0.0f;
                    delay = Random.Range(delayRange.x, delayRange.y);
                }
            }
        } else {
            counter = 0.0f;
            if (all_coins.Count != 0) {
                for (int i = 0; i < all_coins.Count; i++) {
                    if (all_coins[i] != null) {
                        Destroy(all_coins[i].gameObject);
                    }
                }
            }

            all_coins = new List<GameObject>();
            maxCoinCounter = 0;
            delay = Random.Range(delayRange.x, delayRange.y);

            this.GetComponent<CoinController>().resetCurrentCoinText();
        }
    }

    private void spawnNewCoin() {
        float random = Random.Range(-gamecontroller.GetComponent<BallController>().randRange.x, gamecontroller.GetComponent<BallController>().randRange.y);
        GameObject obj = Instantiate(coin_prefab, startPositionPrefab + new Vector3(0, random, 0), coin_prefab.transform.rotation) as GameObject;
        all_coins.Add(obj);
        maxCoinCounter++;
    }
}
