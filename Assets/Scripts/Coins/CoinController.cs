﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CoinController : MonoBehaviour {

    [SerializeField]
    private GameObject CurrentCoins_obj;
    [SerializeField]
    private GameObject TotalCoins_obj;

    private int currentCoins = 0;
    private int totalCoins = 0;

    public int getTotalCoins() { return totalCoins; }

    private string totalcoinspath = "totalcoins";


    // Use this for initialization
    void Start () {
        string value = SaveTxt.Instance().Load(totalcoinspath);
        if (value.Equals("")) {
            SaveTxt.Instance().Save(totalcoinspath, "0");
        }
        totalCoins = stringToInt(value);
        currentCoins = 0;
        CurrentCoins_obj.GetComponent<Text>().text = currentCoins.ToString();
    }

    // Update is called once per frame
    void Update () {
        if (!TotalCoins_obj.GetComponent<Text>().text.Equals(totalCoins.ToString())) {
            TotalCoins_obj.GetComponent<Text>().text = "Coins: " + totalCoins.ToString();
        }
    }

    private int stringToInt(string str) {
        if (str.Equals("")) {
            return 0;
        }
        return int.Parse(str);
    }

    public void addCoin() {
        currentCoins += 1;
        totalCoins += 1;

        CurrentCoins_obj.GetComponent<Text>().text = currentCoins.ToString();
        SaveTxt.Instance().Save(totalcoinspath, totalCoins.ToString());
    }

    public void rewardPlayerFromAD() {
        totalCoins += 5;
        SaveTxt.Instance().Save(totalcoinspath, totalCoins.ToString());
    }

    public void minusTotalPrice(int value) {
        totalCoins -= value;
        currentCoins = 0;

        SaveTxt.Instance().Save(totalcoinspath, totalCoins.ToString());
    }

    public void resetCurrentCoinText() {
        currentCoins = 0;
    }
}
