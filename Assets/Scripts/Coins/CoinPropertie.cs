﻿using UnityEngine;
using System.Collections;

public class CoinPropertie : MonoBehaviour {

    [SerializeField]
    private float movementSpeed;
    private GameObject coincontroller;

    private bool isPickedUp = false;
    public bool getIsPickedUp() { return isPickedUp; }

    // Use this for initialization
    void Start() {
        coincontroller = GameObject.FindObjectOfType<CoinController>().gameObject;
    }

    // Update is called once per frame
    void Update() {
        this.transform.position += -this.transform.right * movementSpeed * Time.deltaTime;

        if (this.transform.position.x <= -3.75) {
            Destroy(this.gameObject);
        }
    }

    public void OnCollisionEnter(Collision other) {
        if (other.gameObject.tag.Equals("Racket") && !isPickedUp) {
            coincontroller.GetComponent<CoinController>().addCoin();
            isPickedUp = true;
            Destroy(this.gameObject);
        }
    }
}
