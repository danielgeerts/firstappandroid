﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SettingsController : MonoBehaviour {

    [SerializeField]
    private List<GameObject> MoveObjects_1;
    [SerializeField]
    private List<GameObject> MoveObjects_2;

    [SerializeField]
    private int Time;

    private List<List<GameObject>> ObjectWithOldNewPositions;


    private bool SettingsIsClicked = false;


	// Use this for initialization
	void Start () {
        ObjectWithOldNewPositions = new List<List<GameObject>>();
        ObjectWithOldNewPositions.Add(MoveObjects_1);
        ObjectWithOldNewPositions.Add(MoveObjects_2);
    }

    // Update is called once per frame
    void Update () {
        if (SettingsIsClicked) {
            for (int i = 0; i < ObjectWithOldNewPositions.Count; i++) {
                ObjectWithOldNewPositions[i][0].transform.position = Vector3.Lerp(ObjectWithOldNewPositions[i][0].transform.position, ObjectWithOldNewPositions[i][2].transform.position, Time * UnityEngine.Time.deltaTime);
            }
        } else {
            for (int i = 0; i < ObjectWithOldNewPositions.Count; i++) {
                ObjectWithOldNewPositions[i][0].transform.position = Vector3.Lerp(ObjectWithOldNewPositions[i][0].transform.position, ObjectWithOldNewPositions[i][1].transform.position, Time * UnityEngine.Time.deltaTime);
            }
        }
    }

    public void SettingsButtonOnCLick() {
        SettingsIsClicked = !SettingsIsClicked;
    }
}
