﻿using UnityEngine;
using System.Collections;

public class MoveTouchIcon : MonoBehaviour {

    [SerializeField]
    private GameObject Finger_Icon;
    [SerializeField]
    private float newRotationX;

    [SerializeField]
    private float timer;
    private float counter = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        counter += Time.deltaTime;

        if (counter >= timer) {
            Vector3 rotationVector = Finger_Icon.transform.rotation.eulerAngles;
            
            if (Finger_Icon.transform.localEulerAngles.x < newRotationX) {
                rotationVector.x = newRotationX +1;
                Finger_Icon.transform.rotation = Quaternion.Euler(rotationVector);
            } else {
                rotationVector.x = 0.0f;
                Finger_Icon.transform.rotation = Quaternion.Euler(rotationVector);
            }
            counter = 0;
        }
	}
}
