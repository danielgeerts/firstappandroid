﻿using UnityEngine;
using System.Collections;
using admob;
using GoogleMobileAds.Api;

public class GoogleAdManager : MonoBehaviour {

    public static GoogleAdManager Instance { set; get; }

    [SerializeField]
    private int maxCount = 5;
    private int counter;

    public bool isShowingBanner = false;

    private RewardBasedVideoAd rewardBasedVideo;

    private void Start() {
        CheckIfUsingOwnDeviceWithGooglePlay();

        Instance = this;
        //DontDestroyOnLoad(this.gameObject);

        initAdmob();
        ShowBanner();

        counter = 0;
    }

    private float counterForChecking = 0.0f;
    private float delayForChecking = 1.0f;

    public void Update() {
        counterForChecking += 1 * Time.deltaTime;
        if (counterForChecking > delayForChecking) {
            CheckIfUsingOwnDeviceWithGooglePlay();
            counterForChecking = 0;
        }
    }

    private string GetUniquePhoneID() {
        return SystemInfo.deviceUniqueIdentifier;
    }

    // When the developer using a phone with "DemonWolfHunter" Google Play Username then set the ads to Testing
    // Need to do this because Google AdMob says so, something with cheating when you dont do this
    public void CheckIfUsingOwnDeviceWithGooglePlay() {
        Debug.Log("Current Google Play userName: " + Social.localUser.userName);

        if (Social.localUser.userName.Equals("DemonWolfHunter")) {
            ad.setTesting(true);
        }
    }

    public void ShowBanner() {
        admob.AdSize adSize = new admob.AdSize(360, 50);
        Admob.Instance().showBannerRelative(adSize, admob.AdPosition.TOP_CENTER, 0);
        isShowingBanner = true;
    }

    public void ShowVideo() {
        /*if (ad.isRewardedVideoReady()) {
            ad.showRewardedVideo();
        } else {
            ad.loadRewardedVideo("ca-app-pub-1647593060167026/1077693792");
        }*/

        RequestRewardBasedVideo();

        if (rewardBasedVideo.IsLoaded()) {
            rewardBasedVideo.Show();
        }

    }

    Admob ad;
    void initAdmob() {
        ad = Admob.Instance();
        ad.bannerEventHandler += onBannerEvent;
        ad.interstitialEventHandler += onInterstitialEvent;
        /*
        // Ad event fired when the rewarded video ad
        // has been received.
        rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;
        // has failed to load.
        rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
        // is opened.
        rewardBasedVideo.OnAdOpening += HandleRewardBasedVideoOpened;
        // has started playing.
        rewardBasedVideo.OnAdStarted += HandleRewardBasedVideoStarted;
        // has rewarded the user.
        rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
        // is closed.
        rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
        // is leaving the application.
        rewardBasedVideo.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;
        */

        ad.initAdmob("ca-app-pub-1647593060167026/9661052599", "ca-app-pub-1647593060167026/3614518996");
        ad.setTesting(true);
        Debug.Log("admob inited -------------");
    }

    void HandleRewardBasedVideoRewarded(object sender, Reward args) {
        //Debug.Log("handler onAdmobEvent---" + eventName + "   " + msg);
        GameObject.FindObjectOfType<CoinController>().rewardPlayerFromAD();
    }

    private void RequestRewardBasedVideo() {
        string adUnitId = "ca-app-pub-1647593060167026/1077693792";

        rewardBasedVideo = RewardBasedVideoAd.Instance;

        AdRequest request = new AdRequest.Builder().Build();
        rewardBasedVideo.LoadAd(request, adUnitId);
    }


    void onInterstitialEvent(string eventName, string msg) {
        Debug.Log("handler onAdmobEvent---" + eventName + "   " + msg);
        if (eventName == AdmobEvent.onAdLoaded) {
            Admob.Instance().showInterstitial();
        }
    }
    void onBannerEvent(string eventName, string msg) {
        Debug.Log("handler onAdmobBannerEvent---" + eventName + "   " + msg);
    }
}
