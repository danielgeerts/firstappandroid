﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class OverallColorForSprite : MonoBehaviour {

    [SerializeField]
    private Material overallMaterial;
    [SerializeField]
    private Material spritesMaterial;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (spritesMaterial.color != overallMaterial.color) {
            spritesMaterial.color = overallMaterial.color;
        }
	}
}
