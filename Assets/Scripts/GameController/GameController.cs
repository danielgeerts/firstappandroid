﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

    [SerializeField]
    private GameObject Vangnet;
    [SerializeField]
    private GameObject ScoreGameScreen;
    [SerializeField]
    private GameObject ScoreWinScreen;
    [SerializeField]
    private GameObject MenuCanvas;
    [SerializeField]
    private GameObject WinCanvas;
    [SerializeField]
    private GameObject SettingsCanvas;
    [SerializeField]
    private GameObject ChangeModelCanvas;
    [SerializeField]
    private GameObject InformationCanvas;

    private BallController ballController;

    private bool isMenu = true;

    public bool canSpawnCoin = false;

    // Use this for initialization
    void Start() {
        Vangnet.GetComponent<ExpendTennisRacket>().enabled = false;
        ballController = this.GetComponent<BallController>();

        ScoreGameScreen.SetActive(false);
        MenuCanvas.SetActive(true);
        WinCanvas.SetActive(false);
        SettingsCanvas.SetActive(true);
        ChangeModelCanvas.SetActive(false);
        InformationCanvas.SetActive(false);
    }

    // Update is called once per frame
    void Update() {
        // When the Back Button is pressed on an Android device
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (MenuCanvas.activeSelf) {
                Application.Quit();
            } else {
                ScoreGameScreen.SetActive(false);
                MenuCanvas.SetActive(true);
                WinCanvas.SetActive(false);
                SettingsCanvas.SetActive(true);
                ChangeModelCanvas.SetActive(false);
                InformationCanvas.SetActive(false);

                this.gameObject.GetComponent<BallController>().ResetBallController();

                canSpawnCoin = false;
            }
        }
    }

    public void StartGame() {
        Vangnet.GetComponent<ExpendTennisRacket>().enabled = true;
        Vangnet.GetComponent<ScoreController>().RestartScore();
        ballController.SetCountTimeEqualToMaxTime();

        ScoreGameScreen.SetActive(true);
        MenuCanvas.SetActive(false);
        WinCanvas.SetActive(false);
        SettingsCanvas.SetActive(false);
        ChangeModelCanvas.SetActive(false);
        InformationCanvas.SetActive(false);

        isMenu = false;
        canSpawnCoin = true;
    }

    public void StopGame() {
        this.gameObject.GetComponent<ColorController>().setColorToRandom();
        ballController.ResetBallController();

        foreach (BallMovement obj in GameObject.FindObjectsOfType<BallMovement>()) {
            Destroy(obj.gameObject);
        }

        Vangnet.GetComponent<ExpendTennisRacket>().RestartVangnet();

        Vangnet.GetComponent<ExpendTennisRacket>().enabled = false;
        Vangnet.GetComponent<ScoreController>().SaveGameScore();

        ScoreGameScreen.SetActive(false);
        MenuCanvas.SetActive(false);
        WinCanvas.SetActive(true);
        SettingsCanvas.SetActive(true);
        ChangeModelCanvas.SetActive(false);
        InformationCanvas.SetActive(false);

        isMenu = false;
        canSpawnCoin = false;
    }

    public void pauseOrUnpauseGame() {
        if (Time.timeScale == 1.0f) {
            Time.timeScale = 0.0f;
        } else {
            Time.timeScale = 1.0f;
        }
    }

    public void BackToStartCanvas() {
        ScoreGameScreen.SetActive(false);
        MenuCanvas.SetActive(true);
        WinCanvas.SetActive(false);
        SettingsCanvas.SetActive(true);
        ChangeModelCanvas.SetActive(false);
        InformationCanvas.SetActive(false);

        isMenu = true;
    }

    public void OpenStartFromSettingsCanvas() {
        ScoreGameScreen.SetActive(false);
        if (isMenu) {
            MenuCanvas.SetActive(true);
            WinCanvas.SetActive(false);
        } else {
            MenuCanvas.SetActive(false);
            WinCanvas.SetActive(true);
        }
        SettingsCanvas.SetActive(true);
        ChangeModelCanvas.SetActive(false);
        InformationCanvas.SetActive(false);

        canSpawnCoin = false;
    }

    public void OpenChangeModelCanvas() {
        ScoreGameScreen.SetActive(false);
        MenuCanvas.SetActive(false);
        WinCanvas.SetActive(false);
        SettingsCanvas.SetActive(true);
        ChangeModelCanvas.SetActive(true);
        InformationCanvas.SetActive(false);

        canSpawnCoin = false;
    }

    public void OpenInformationCanvas() {
        ScoreGameScreen.SetActive(false);
        MenuCanvas.SetActive(false);
        WinCanvas.SetActive(false);
        SettingsCanvas.SetActive(true);
        ChangeModelCanvas.SetActive(false);
        InformationCanvas.SetActive(true);

        canSpawnCoin = false;
    }
}
