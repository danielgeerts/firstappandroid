﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class SelectModel : MonoBehaviour {

    [SerializeField]
    private List<GameObject> Buttons;
    private List<GameObject> Models;

    private Color standardColor = new Color(150f/255f, 255f/255f, 244f/255f);
    private Color newColor = Color.blue;

    // Use this for initialization
    void Start() {
        string fileValue = SaveTxt.Instance().Load("CurrentModel");
        if (fileValue.Equals("")) {
            SaveTxt.Instance().Save("CurrentModel", Buttons[0].GetComponent<ModelBtnProperties>().getModelFromButton().gameObject.name);
        }
        fileValue = fileValue.Trim();

        SelectModelTroughName(fileValue);
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SelectModelTroughName(string newmodel_str) {
        string newModelName = "";
        bool somethingWentWrong = false;

        for (int i = 0; i < Buttons.Count; i++) {
            if (Buttons[i].GetComponent<ModelBtnProperties>().getModelFromButton().name.Equals(newmodel_str)) {
                Buttons[i].GetComponent<Image>().color = newColor;
                Buttons[i].GetComponent<ModelBtnProperties>().getModelFromButton().gameObject.SetActive(true);
                newModelName = Buttons[i].GetComponent<ModelBtnProperties>().getModelFromButton().gameObject.name;
                somethingWentWrong = false;

                int temp = i + 1;
                if (temp < Buttons.Count) {
                    for (int j = temp; j < Buttons.Count; j++) {
                        Buttons[j].GetComponent<Image>().color = standardColor;
                        Buttons[j].GetComponent<ModelBtnProperties>().getModelFromButton().gameObject.SetActive(false);
                    }
                }

                break;
            } else {
                Buttons[i].GetComponent<Image>().color = standardColor;
                Buttons[i].GetComponent<ModelBtnProperties>().getModelFromButton().gameObject.SetActive(false);
                somethingWentWrong = true;
            }
        }

        if (somethingWentWrong) {
            if (Buttons[0] != null) {
                Buttons[0].GetComponent<Image>().color = newColor;
                Buttons[0].GetComponent<ModelBtnProperties>().getModelFromButton().SetActive(true);
                newModelName = Buttons[0].GetComponent<ModelBtnProperties>().getModelFromButton().gameObject.name;
            }
        }

        SaveTxt.Instance().Save("CurrentModel", newModelName.Trim());
    }
}
