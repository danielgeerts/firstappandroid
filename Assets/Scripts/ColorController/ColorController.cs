﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ColorController : MonoBehaviour {

    [SerializeField]
    private Material overallColor;
    [SerializeField]
    private Material background;
    [SerializeField]
    private GameObject objWithParticleSystem;
    private ParticleSystem particlesystem;

    private List<Color> AllColors;
    private int currentElement;
    private float fakeTime = 0;

    // Use this for initialization
    void Start () {
        AllColors = new List<Color>();
        AllColors.Add(Color.red);
        AllColors.Add(Color.magenta);
        AllColors.Add(Color.blue);
        AllColors.Add(Color.cyan);
        AllColors.Add(Color.green);
        AllColors.Add(Color.yellow);

        currentElement = Random.Range(0, AllColors.Count);

        overallColor.color = AllColors[currentElement];

        particlesystem = objWithParticleSystem.GetComponentInChildren<ParticleSystem>();
    }
	
	// Update is called once per frame
	void Update () {
        particlesystem.startColor = overallColor.color;
    }

    public void changeColor() {
        if (overallColor.color == AllColors[currentElement]) {
            currentElement++;
            fakeTime = 0;
            if (currentElement >= AllColors.Count) {
                currentElement = 0;
            }
        }
        fakeTime += 0.1f;

        overallColor.color = Color.Lerp(overallColor.color, AllColors[currentElement], fakeTime);
    }

    public void setColorToRandom() {
        fakeTime = 0;

        currentElement = Random.Range(0, AllColors.Count);

        overallColor.color = AllColors[currentElement];
    }
}
