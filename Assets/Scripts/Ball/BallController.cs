﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BallController : MonoBehaviour {

    [SerializeField]
    private GameObject ButterflyPrefab;
    public Vector3 startPosition;

    [SerializeField]
    private float timeNextSpawn; 
    private float timeCounter;

    [SerializeField]
    public Vector2 randRange;
    [SerializeField]
    private Vector2 randRangeBanner;

    [SerializeField]
    private float addableBallMovement;
    private float totalNewBallMovement;

    private bool spawning = false;

    // Use this for initialization
    void Start () {
        startPosition = ButterflyPrefab.transform.position;

        timeCounter = timeNextSpawn;
    }
	
	// Update is called once per frame
	void Update () {
        if (spawning) {
            timeCounter += Time.deltaTime;
            if (timeCounter >= timeNextSpawn) {
                float random;
                if (this.GetComponent<GoogleAdManager>().isShowingBanner) {
                } else {
                    random = Random.Range(-randRangeBanner.x, randRangeBanner.y);
                }
                random = Random.Range(-randRange.x, randRange.y);

                GameObject temp = Instantiate(ButterflyPrefab, startPosition + new Vector3(0, random, 0), ButterflyPrefab.transform.rotation) as GameObject;
                timeCounter = 0.0f;
                totalNewBallMovement += addableBallMovement;
                temp.GetComponent<BallMovement>().addSpeedToMovement(totalNewBallMovement);
            }
        }
    }

    public void SetCountTimeEqualToMaxTime() {
        timeCounter = timeNextSpawn;
        spawning = true;
    }

    public void ResetBallController() {
        totalNewBallMovement = 0;
        spawning = false;
    }
}
