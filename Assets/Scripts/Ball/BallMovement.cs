﻿using UnityEngine;
using System.Collections;

public class BallMovement : MonoBehaviour {
    
    [SerializeField]
    private float movementSpeedStart;
    private float movementSpeed;

    public bool isTurned = false;

    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
        this.transform.position += -this.transform.right * movementSpeed * Time.deltaTime;

        if (this.transform.position.x <= -3.75) {
            GameObject.FindObjectOfType<GameController>().StopGame();
        } else if (this.transform.position.x >= 5) {
            Destroy(this.gameObject);
        }
    }

    public void Turn() {
        if (this.gameObject.transform.rotation.y <= 10.0f && this.gameObject.transform.rotation.y >= -10.0f) {
            this.gameObject.transform.rotation = new Quaternion(this.gameObject.transform.rotation.x, 180.0f, this.gameObject.transform.rotation.z, this.gameObject.transform.rotation.w);
        } else if (this.gameObject.transform.rotation.y <= 190.0f && this.gameObject.transform.rotation.y >= 170.0f) {
            this.gameObject.transform.rotation = new Quaternion(this.gameObject.transform.rotation.x, 0.0f, this.gameObject.transform.rotation.z, this.gameObject.transform.rotation.w);
        }

        isTurned = true;

        this.gameObject.GetComponentInChildren<ParticleSystem>().Stop();
        this.gameObject.GetComponentInChildren<ParticleSystem>().Clear();
        this.gameObject.GetComponentInChildren<ParticleSystem>().Play();
    }

    public void addSpeedToMovement(float newSpeed) {
        movementSpeed = movementSpeedStart;
        movementSpeed += newSpeed;
        Debug.Log("Ball Movement: " + movementSpeed);
    }

    public void OnCollisionStay(Collision other) {
        if (other.gameObject.tag.Equals("TrowBall")) {
            Vector3 temp = this.gameObject.transform.position;
            if (isTurned) {
                temp.y += Time.deltaTime;
            } else {
                temp.y -= Time.deltaTime;
            }
            this.gameObject.transform.position = temp;
        }
    }
}
