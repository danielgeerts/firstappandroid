using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine.UI;

public class ShareAndRate : MonoBehaviour {

    [SerializeField]
    private GameObject player;
    [SerializeField]
    private string subject = "Deflector Android App";
    [SerializeField]
    private string body = "This is my score, can u beat me in the fun new app Deflector? Try it out, now for free!";
    [SerializeField]
    private string marketShortcutLink = "market://details?id=pub-1647593060167026";

#if UNITY_IPHONE
	
	[DllImport("__Internal")]
	private static extern void sampleMethod (string iosPath, string message);
	
	[DllImport("__Internal")]
	private static extern void sampleTextMethod (string message);
	
#endif

    private AndroidJavaClass intentClass;
    private AndroidJavaObject intentObject;

    void Start() {
        //Reference of AndroidJavaClass class for intent
        intentClass = new AndroidJavaClass("android.content.Intent");
        //Reference of AndroidJavaObject class for intent
        intentObject = new AndroidJavaObject("android.content.Intent");
    }

    public void OnAndroidTextSharingClick()
	{
		
		StartCoroutine(ShareAndroidText());
		
	}
	IEnumerator ShareAndroidText()
	{
		yield return new WaitForEndOfFrame();
        //execute the below lines if being run on a Android device
#if UNITY_ANDROID

        subject = "Deflector Android App";
        string link = "https://play.google.com/store/apps/details?id=com.DanielGeerts.Deflector";
        string endstring = "My score on Deflector is, can u beat me in the fun new app Deflector?\n\nTry it out, now for free! Click here to download Deflector!\n\n" + link;
        body = endstring.Replace("My score on Deflector is", "My score on Deflector is " + player.GetComponent<ScoreController>().GetWinScoreString());

        
		//call setAction method of the Intent object created
		intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		//set the type of sharing that is happening
		intentObject.Call<AndroidJavaObject>("setType", "text/plain");
		//add data to be passed to the other activity i.e., the data to be sent
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), subject);
		//intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TITLE"), "Text Sharing ");
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), body);
		//get the current activity
		AndroidJavaClass unity = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
		//start the activity by sending the intent data
		AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, "Share Via");
		currentActivity.Call("startActivity", jChooser);
#endif
}


public void OniOSTextSharingClick()
	{
		
		#if UNITY_IPHONE || UNITY_IPAD
		string shareMessage = "Wow I Just Share Text ";
		sampleTextMethod (shareMessage);
		
		#endif
	}

	public void RateUs()
	{
		#if UNITY_ANDROID
		Application.OpenURL(marketShortcutLink);
		#elif UNITY_IPHONE
		Application.OpenURL("itms-apps://itunes.apple.com/app/idYOUR_ID");
		#endif
	}

}
