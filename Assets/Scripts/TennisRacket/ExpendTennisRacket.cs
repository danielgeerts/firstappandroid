﻿using UnityEngine;
using System.Collections;

public class ExpendTennisRacket : MonoBehaviour {

    [SerializeField]
    private GameObject game_controller;

    [SerializeField]
    private GameObject Vangnet_Stok;
    [SerializeField]
    private GameObject Vangnet_Net;
    [SerializeField]
    private GameObject Vangnet_Net_newPos;

    [SerializeField]
    private float maxHeight = 6.0f;
    [SerializeField]
    private float maxHeightBanner = 7.5f;
    [SerializeField]
    private float growspeed;
    [SerializeField]
    private float maxspeed;
    [SerializeField]
    private float addOnPerSecond;

    private float beginspeed;

    private Vector3 beginScale;
    private Vector3 beginPosition;

    // Use this for initialization
    void Start () {
        beginScale = Vangnet_Stok.transform.localScale;
        beginPosition = Vangnet_Net.transform.position;

        beginspeed = growspeed;
    }
	
	// Update is called once per frame
	void Update () {
        if (growspeed < maxspeed) {
            growspeed += addOnPerSecond * Time.deltaTime;
        }

        if (Input.touchCount > 0 || Input.GetMouseButton(0)) {
            float currentheight;
            if (game_controller.GetComponent<GoogleAdManager>().isShowingBanner) {
                currentheight = maxHeight;
            } else {
                currentheight = maxHeightBanner;
            }
            if (Vangnet_Stok.transform.localScale.y < currentheight) {
                Vangnet_Stok.transform.localScale += new Vector3(0, growspeed * Time.deltaTime, 0);
            }

            Vangnet_Net.transform.position = Vangnet_Net_newPos.transform.position;
        } else {
            if (Vangnet_Stok.transform.localScale.y > beginScale.y) {
                Vangnet_Stok.transform.localScale -= new Vector3(0, growspeed * Time.deltaTime, 0);
            } else {
                Vangnet_Stok.transform.localScale = beginScale;
            }

            Vangnet_Net.transform.position = Vangnet_Net_newPos.transform.position;
        }
    }

    public void RestartVangnet() {
        Vangnet_Stok.transform.localScale = beginScale;
        Vangnet_Net.transform.position = beginPosition;

        growspeed = beginspeed;
    }
}
