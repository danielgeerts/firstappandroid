﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ModelBtnProperties : MonoBehaviour {

    [SerializeField]
    private GameObject model_object;
    [SerializeField]
    private GameObject select_model_Controller;
    public GameObject getModelFromButton() { return model_object; }

    [SerializeField]
    private GameObject main_img;
    [SerializeField]
    private GameObject locked_img;
    [SerializeField]
    private GameObject show_price_text;

    [SerializeField]
    private int unlock_coin_price;

    [SerializeField]
    private GameObject coin_controller;

    [SerializeField]
    private bool isFirstFreeObj = false;

    private bool isLocked = true;
    public bool getIsLocked() { return isLocked; }

    // Use this for initialization
    void Start() {
        main_img.SetActive(false);
        locked_img.SetActive(true);

        //SaveTxt.Instance().Save("button_" + this.gameObject.name.Trim(), isLocked.ToString());

        string temp = SaveTxt.Instance().Load("button_" + this.gameObject.name.Trim());

        if (temp.Equals("")) {
            if (isFirstFreeObj) {
                SaveTxt.Instance().Save("button_" + this.gameObject.name.Trim(), "False");
                this.setLocked(false);
            } else {
                SaveTxt.Instance().Save("button_" + this.gameObject.name.Trim(), "True");
                this.setLocked(true);
            }
        } else {
            this.setLocked(this.stringToBool(temp));
        }

        show_price_text.GetComponent<Text>().text = unlock_coin_price.ToString();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    private void setLocked(bool value) {
        if (value) {
            this.gameObject.GetComponent<Button>().interactable = false;
            main_img.SetActive(false);
            locked_img.SetActive(true);
            isLocked = true;
        } else {
            this.gameObject.GetComponent<Button>().interactable = true;
            main_img.SetActive(true);
            locked_img.SetActive(false);
            isLocked = false;
        }
    }

    private bool stringToBool(string str) {
        if (!str.ToLower().Contains("true")) {
            return false;
        }
        return true;
    }

    public void CurrentButtonClicked() {
        select_model_Controller.GetComponent<SelectModel>().SelectModelTroughName(model_object.name);
    }

    public void tryUnlock() {
        if (coin_controller.GetComponent<CoinController>().getTotalCoins() >= unlock_coin_price) {
            coin_controller.GetComponent<CoinController>().minusTotalPrice(unlock_coin_price);
            setLocked(false);

            SaveTxt.Instance().Save("button_" + this.gameObject.name.Trim(), isLocked.ToString());
        }
    }
}
