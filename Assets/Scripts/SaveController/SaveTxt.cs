﻿using UnityEngine;
using System.Collections;
using System.IO;

public class SaveTxt : MonoBehaviour {

    private static SaveTxt _instance;

    public static SaveTxt Instance() {
        if (_instance == null) {
            _instance = new SaveTxt();
        }
        return _instance;
    }

    //private string encrypteValue = "njk9miOiRt52kRc83zEFwmWLaV+ird5VaOHg8VZdb9Boe7r2e+2tM+GLtXzXLROJiBQ88hXDTccM28F8h7YiNJlFgEZXLFfJPExD16ksItGdqCsmENIMU6mYib55+rqo";

    public string pathToFile = Application.persistentDataPath + "/" + "JsonSettings.json";

    public void Save(string fileName, string sendData) {

        //string password = "12345";
        //string plaintext = "test123";

       // string encryptedstring = EncryptFile.Encrypt(plaintext, password);
        //string decryptedstring = EncryptFile.Decrypt(encryptedstring, password);

        string tosend = Singleton<WriteJson>.Instance.addStringToJsonStringAndReturn(fileName, sendData);

        CreateFile();

        File.WriteAllText(pathToFile, tosend);
    }

    public string Load(string key) {

        //string password = "12345";
        //string plaintext = "test123";

        //string encryptedstring = EncryptFile.Encrypt(plaintext, password);
        //string decryptedstring = EncryptFile.Decrypt(encryptedstring, password);

        //if (encryptedstring.Equals(encrypteValue)) {
            if (!File.Exists(pathToFile)) {
                CreateFile();
            }
            //string value = WriteJson.getValueTroughKey(key);
            string value = Singleton<WriteJson>.Instance.getValueTroughKey(key);
        //}
        return value;
    }

    public string getFileValue() {
        return File.ReadAllText(pathToFile);

    }

    private void CreateFile() {
        if (!File.Exists(pathToFile)) {
            FileStream fs = File.Create(pathToFile);
            fs.Close();
        }
    }

    public bool CheckPath() {
        return File.Exists(pathToFile);
    }
}
