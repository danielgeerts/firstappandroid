﻿using UnityEngine;
using System.Collections;
using LitJson;
using System.IO;
using System.Collections.Generic;

public class Singleton<WriteJson> : MonoBehaviour where WriteJson : Singleton<WriteJson> {
    public static WriteJson Instance { get; private set; }

    protected virtual void Awake() {
        if (Instance == null) {
            Instance = (WriteJson)this;
        } else {
            Debug.LogError("Got a second instance of the class " + this.GetType());
        }
    }
}

public class WriteJson : Singleton<WriteJson> {

    //private JsonData jsondata;
    private JsonData jsonsettings;

    private string databasename = "settings";

    // Use this for initialization
    void Start() {
        if (jsonsettings == null || jsonsettings["settings"] == null || jsonsettings["settings"].Count == 0) {
            string jsonstring = File.ReadAllText(SaveTxt.Instance().pathToFile);
            if (jsonstring == null || jsonstring.Equals("")) {
                jsonstring = "{\"" + databasename + "\":[]}";
            }

            jsonsettings = JsonMapper.ToObject(jsonstring);
        }
    }

    public string addStringToJsonStringAndReturn(string keyname, string data) {
        if (jsonsettings == null || jsonsettings["settings"] == null || jsonsettings["settings"].Count == 0) {
            string jsonstring = File.ReadAllText(SaveTxt.Instance().pathToFile);
            if (jsonstring == null || jsonstring.Equals("")) {
                jsonstring = "{\"" + databasename + "\":[{\"objectName\":\"" + keyname + "\",\"objectValue\":\"" + data + "\"}]}";
            }

            jsonsettings = JsonMapper.ToObject(jsonstring);
        }

        string jsonstr = "";
        bool keyNotFound = false;

        if (jsonsettings["settings"].Count > 0) {
            for (int i = 0; i < jsonsettings["settings"].Count; i++) {
                if (jsonsettings["settings"][i]["objectName"].Equals(keyname)) {
                    jsonsettings["settings"][i]["objectValue"] = data;
                    keyNotFound = true;
                }
            }
        }

        if (!keyNotFound) {
            int newpos = jsonsettings["settings"].Count;
            JsonSettings settings = new JsonSettings();
            if (settings.settings == null) {
                settings.settings = new List<settings>();
            }

            for (int i = 0; i < jsonsettings["settings"].Count; i++) {
                settings.settings.Add(new settings(jsonsettings["settings"][i]["objectName"].ToString(), jsonsettings["settings"][i]["objectValue"].ToString()));
            }

            settings.settings.Add(new settings(keyname, data));

            jsonsettings = JsonMapper.ToObject(JsonMapper.ToJson(settings));
        }

        jsonstr = "{\"" + databasename + "\":[";
        int counter = 0;

        string testjuh = jsonsettings.ToJson();

        for (int j = 0; j < jsonsettings["settings"].Count; j++) {
            counter++;

            jsonstr += "{\"objectName\":\"" + jsonsettings["settings"][j]["objectName"].ToString();
            jsonstr += "\",\"objectValue\":\"";

            string value = jsonsettings["settings"][j]["objectValue"].ToString();

            if (counter >= jsonsettings["settings"].Count) {
                jsonstr += value + "\"}";
            } else {
                jsonstr += value + "\"}" + ",";
            }
        }

        jsonstr += "]}";

        return jsonstr;
    }

    public string getValueTroughKey(string key) {
        string str = "";

        if (jsonsettings == null || jsonsettings["settings"] == null || jsonsettings["settings"].Count == 0) {
            string jsonstring = File.ReadAllText(SaveTxt.Instance().pathToFile);
            if (jsonstring == null || jsonstring.Equals("")) {
                jsonstring = "{\"" + databasename + "\":[]}";
            }

            jsonsettings = JsonMapper.ToObject(jsonstring);
        }

        if (jsonsettings["settings"].Count > 0) {
            for (int i = 0; i < jsonsettings["settings"].Count; i++) {
                if (jsonsettings["settings"][i]["objectName"].Equals(key)) {
                    str = jsonsettings["settings"][i]["objectValue"].ToString();
                    break;
                }
            }
        }

        return str;
    }
}

public static class AddToArray {
    public static settings[] AddItemToArray(this settings[] original, settings itemToAdd) {
        settings[] finalArray = new settings[original.Length + 1];
        for (int i = 0; i < original.Length; i++) {
            finalArray[i] = original[i];
        }
        finalArray[finalArray.Length - 1] = itemToAdd;
        return finalArray;
    }
}

public class JsonSettings {
    public List<settings> settings { get; set; }

    public JsonSettings() {
        settings = new List<settings>();
    }
}

public class settings : object {
    public string objectName { get; set; }
    public string objectValue { get; set; }

    public settings(string keyname, string data) {
        this.objectName = keyname;
        this.objectValue = data;
    }
}